import { createContext } from 'react';

const AvatarContext = createContext({
    avatarName: '',
    setAvatarName: () => {},
});

export default AvatarContext;
