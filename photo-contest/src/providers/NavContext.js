import { createContext } from 'react';

const NavContext = createContext({
    isActive: false,
    setActiveState: () => {},
});

export default NavContext;
