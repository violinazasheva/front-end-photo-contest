import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import decode from 'jwt-decode';
import NotFound from './components/NotFound/NotFound';
import JunkiesDashboard from './components/PrivatePage/JunkiePage/JunkiesDashboard';
import ContestsInPhase from './components/PrivatePage/OrganizerPage/Dashboard/ContestsInPhase';
import OrganizersDashboard from './components/PrivatePage/OrganizerPage/Dashboard/OrganizersDashboard';
import HomePage from './components/PublicPage/Home/HomePage';
import SignIn from './components/PublicPage/SignIn/SignIn';
import SignUp from './components/PublicPage/SignUp/SignUp';
import AuthContext from './providers/AuthContext';
import GuardedRoute from './providers/GuardedRoute';
import Navbar from './components/PublicPage/Navbar/Navbar';
import CreateContest from './components/PrivatePage/OrganizerPage/CreateContest/CreateContest';
import ViewInvitational from './components/PrivatePage/JunkiePage/ViewInvitational';
import ViewOpen from './components/PrivatePage/JunkiePage/ViewOpen';
import ViewFinished from './components/PrivatePage/JunkiePage/ViewFinished';
import NavContext from './../src/providers/NavContext'
import JunkieDetails from './components/PrivatePage/JunkiePage/JunkieDetails/JunkieDetails';
import AllPhotos from './components/PrivatePage/Photos/AllPhotos';
import ViewCurrent from './components/PrivatePage/JunkiePage/ViewCurrent';
import ViewJunkiesByRanks from './components/PrivatePage/OrganizerPage/Dashboard/ViewJunkiesByRanks';
import UserProfile from './components/PrivatePage/UserProfile';
import AvatarContext from '../src/providers/AvatarContext';

const App = () => {

  const token = localStorage.getItem('token');
  const [authValue, setAuthValue] = useState({
    isLoggedIn: token ? true : false,
    user: token ? decode(token) : null,
    token: token,
  });

  const [isActive, setActiveState] = useState(true);
  const navbarValue = { isActive, setActiveState };

  const [avatarName, setAvatarName] = useState('');
  const avatarNameValue = { avatarName, setAvatarName };

  return (
    <Router>
      <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
        <NavContext.Provider value={navbarValue}>
          <AvatarContext.Provider value={avatarNameValue}>
            <Navbar key="header" isFirstScreen="enter" isMobile={false} />
            {isActive ? null : authValue.isLoggedIn}
            <Switch>
              <Redirect path="/" exact to="home" />
              <Route path="/home" component={HomePage} />
              <Route path="/signup" component={SignUp} />
              <Route path="/signin" component={SignIn} />
              <GuardedRoute path="/organizers" auth={authValue.isLoggedIn && authValue.user.role === 'Organizator'} exact component={OrganizersDashboard} />
              <GuardedRoute path="/contests/phase/:id" auth={authValue.isLoggedIn && authValue.user.role === 'Organizator'} exact component={ContestsInPhase} />
              <GuardedRoute path="/contests/setup" auth={authValue.isLoggedIn && authValue.user.role === 'Organizator'} exact component={CreateContest} />
              <GuardedRoute path="/contests/invitational" auth={authValue.isLoggedIn && authValue.user.role === 'Junkie'} exact component={ViewInvitational} />
              <GuardedRoute path="/contests/open" auth={authValue.isLoggedIn && authValue.user.role === 'Junkie'} exact component={ViewOpen} />
              <GuardedRoute path="/contests/finished" auth={authValue.isLoggedIn && authValue.user.role === 'Junkie'} exact component={ViewFinished} />
              <GuardedRoute path="/contests/current" auth={authValue.isLoggedIn && authValue.user.role === 'Junkie'} exact component={ViewCurrent} />            
              <GuardedRoute path="/junkies" auth={authValue.isLoggedIn && authValue.user.role === 'Junkie'} exact component={JunkiesDashboard} />
              <GuardedRoute path="/junkies/details" auth={authValue.isLoggedIn && authValue.user.role === 'Junkie'} exact component={JunkieDetails} />
              <GuardedRoute path="/contests/:id/photos/:title" auth={authValue.isLoggedIn} exact component={AllPhotos} />
              <GuardedRoute path="/users/descRanking" auth={authValue.isLoggedIn && authValue.user.role === 'Organizator'} exact component={ViewJunkiesByRanks} />
              <GuardedRoute path="/profile" auth={authValue.isLoggedIn} exact component={UserProfile} />
              <GuardedRoute path="*" component={NotFound} />
            </Switch>
          </AvatarContext.Provider>
        </NavContext.Provider>
      </AuthContext.Provider>
    </Router>
  );
}

export default App;
