import React from 'react';
import { Row, Col } from 'antd';
import './Footer-style.css';

const Footer = () => {
  return (
    <footer id="footer" className="dark">
      <Row className="bottom-bar">
        <Col lg={4} sm={24} />
        <Col lg={20} sm={24}>
          <span
            style={{
              lineHeight: '16px', paddingRight: 12, marginRight: 11, borderRight: '1px solid rgba(255, 255, 255, 0.55)',
            }}
          >
            <a
              href=""
              rel="noopener noreferrer"
              target="_blank"
            >
              Antonina
            </a>
          </span>
          <span style={{ marginRight: 24 }}>
            <a
              href=""
              rel="noopener noreferrer"
              target="_blank"
            >
              Violina
            </a>
          </span>
          <span style={{ marginRight: 12 }}>Copyright © Photo Contest</span>
        </Col>
      </Row>
    </footer>
  );
}
// function Footer() {
//   return (
//     <footer id="footer" className="dark">
//       <Row className="bottom-bar">
//         <Col lg={4} sm={24} />
//         <Col lg={20} sm={24}>
//           <span
//             style={{
//               lineHeight: '16px', paddingRight: 12, marginRight: 11, borderRight: '1px solid rgba(255, 255, 255, 0.55)',
//             }}
//           >
//             <a
//               href=""
//               rel="noopener noreferrer"
//               target="_blank"
//             >
//               Antonina
//             </a>
//           </span>
//           <span style={{ marginRight: 24 }}>
//             <a
//               href=""
//               rel="noopener noreferrer"
//               target="_blank"
//             >
//               Violina
//             </a>
//           </span>
//           <span style={{ marginRight: 12 }}>Copyright © Photo Contest</span>
//         </Col>
//       </Row>
//     </footer>
//   );
// }

export default Footer;
