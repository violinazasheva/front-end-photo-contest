import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Menu, Row, Col, Popover } from 'antd';
import { withRouter } from 'react-router-dom';
import Icon from '@ant-design/icons';
import './Navbar.css';
import NavContext from './../../../providers/NavContext';
import AuthContext from './../../../providers/AuthContext';
import BASE_URL from '../../../common/constants';
import SubMenu from 'antd/lib/menu/SubMenu';
import Swal from 'sweetalert2';
import {
  UserOutlined,
  PlusCircleOutlined,
  StarOutlined,
  CheckCircleOutlined,
  TagsOutlined,
  HomeOutlined,
  PlayCircleOutlined,
  RightSquareOutlined,
  TeamOutlined,
  ClockCircleOutlined
} from '@ant-design/icons';
import AvatarContext from '../../../providers/AvatarContext';


const Navbar = ({ history, isFirstScreen, isMobile }) => {

  const { isLoggedIn, setLoginState, user, token } = useContext(AuthContext);
  const { avatarName, setAvatarName } = useContext(AvatarContext);
  useEffect(() => {
    fetch(`${BASE_URL}/users/avatar`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((result) => result.json())
    .then((data) => {
      setAvatarName(data.avatar)
    })
  }, []);

  const { menuVisible } = true;

  const menuMode = isMobile ? 'inline' : 'horizontal';

  const headerClassName = classNames({
    clearfix: true,
    'home-nav-white': !isFirstScreen,
  });

  const handleSignInButton = () => {
    history.push('/signin');
  };

  const handleLogoutButton = () => {
    Swal.fire({
      icon: 'question',
      title: 'Do you really want to logout?',
      showDenyButton: true,
      confirmButtonText: `Yes`,
      denyButtonText: `No`,
    }).then((result) => {
      if (result.isConfirmed) {
        setLoginState(false);
        setAvatarName('');
        localStorage.removeItem('token');
        Swal.fire('Poof! You just logged out!!', '', 'success');
      } else if (result.isDenied) {
        return null;
      }
    })

  };

  const handleHomeButton = () => {
    if (isLoggedIn) {
      if (user.role === 'Organizator') {
        history.push('/organizers');
      } else if (user.role === 'Junkie') {
        history.push('/junkies');
      }
    } else {
      history.push('/home');
    }
  };

  const handleProfileButton = () => {
    history.push('/profile');
  };

  const handleContestButton = (ev) => {
    const contestType = ev.key;
    history.push(`/contests/${contestType}`)
  }

  const handleSetupContest = () => {
    history.push('/contests/setup');
  };

  const handlePhaseClick = (ev) => {
    const phaseId = +ev.key
    history.push(`/contests/phase/${phaseId}`);
  };

  const handleViewJunkies = () => {
    history.push('/users/descRanking');
  }

  const menu = [
    <Menu mode={menuMode} defaultSelectedKeys={['home']} id="nav" key="nav">
      {(isLoggedIn) ?
        <>
          <Menu.Item onClick={handleProfileButton}>
            <img id='avatar' src={`${BASE_URL}/public/${avatarName.length !== 0 ? avatarName : user.avatar}`} />
            <a>&nbsp;</a><a>&nbsp;</a><a>&nbsp;</a>
            <b>{user.username}</b>
          </Menu.Item>

          {(user.role === "Junkie") ?
            <>
              <SubMenu key="sub1" style={{ 'fontWeight': 'bold' }} title="Contests">
                <Menu.Item key="invitational" icon={<TagsOutlined />} style={{ 'fontWeight': 'bold' }} onClick={handleContestButton}>My invitations</Menu.Item>
                <Menu.Item key="open" icon={<PlayCircleOutlined />} style={{ 'fontWeight': 'bold' }} onClick={handleContestButton}>Open contests</Menu.Item>
                <Menu.Item key="current" icon={<ClockCircleOutlined />} style={{ 'fontWeight': 'bold' }} onClick={handleContestButton}>Current contests</Menu.Item>
                <Menu.Item key="finished" icon={<CheckCircleOutlined />} style={{ 'fontWeight': 'bold' }} onClick={handleContestButton}>Previous contests</Menu.Item>
              </SubMenu>
              {/* <Menu.Item key="4" icon={<UserOutlined />} onClick={handleDetailsButton}>Details</Menu.Item> */}
            </> :
            <>
              <SubMenu key="sub1" style={{ 'fontWeight': 'bold' }} title="Contests">
                <Menu.Item key="11" icon={<PlusCircleOutlined />} onClick={handleSetupContest}>Create new contest</Menu.Item>
                <Menu.Item key="1" icon={<PlayCircleOutlined />} onClick={handlePhaseClick}>Phase 1</Menu.Item>
                <Menu.Item key="2" icon={<RightSquareOutlined />} onClick={handlePhaseClick}>Phase 2</Menu.Item>
                <Menu.Item key="3" icon={<CheckCircleOutlined />} onClick={handlePhaseClick}>Finished</Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" style={{ 'fontWeight': 'bold' }} title="Users">
                <Menu.Item key="13" icon={<TeamOutlined />} style={{ 'fontWeight': 'bold' }} onClick={handleViewJunkies}>Ordered by ranking</Menu.Item>
              </SubMenu>
            </>}
          {/* <Menu.Item key="4" icon={<UserOutlined />} onClick={handleDetailsButton}>Details</Menu.Item> */}

        </>
        :
        null}
      {(!isLoggedIn) ? (
        <Menu.Item id="button-home" key="docs/resource" onClick={handleSignInButton}>
          <b>SIGN IN</b>
        </Menu.Item>) : (
          <Menu.Item id="button-home" key="docs/resource" onClick={handleLogoutButton}>
            <b>SIGN OUT</b>
          </Menu.Item>)}
    </Menu>,
  ];


  return (
    <header id="header-home" className={headerClassName}>
      {menuMode === 'inline' ? (
        <Popover
          overlayClassName="popover-menu"
          placement="bottomRight"
          content={menu}
          trigger="click"
          visible={menuVisible}
          arrowPointAtCenter
        >
          <Icon
            className="nav-phone-icon"
            type="menu"
          />
        </Popover>
      ) : null}
      <Row>
        <Col lg={4} md={5} sm={24} xs={24}>
          <a id="logo" onClick={handleHomeButton}>
            <img alt="logo" src="https://sendeyo.com/up/d/cde8e10236" />
          </a>
        </Col>
        <Col lg={20} md={19} sm={0} xs={0}>
          {menuMode === 'horizontal' ? menu : null}
        </Col>
      </Row>
    </header>
  )
}

export default withRouter(Navbar)

Navbar.propTypes = {
  history: PropTypes.object.isRequired,
  isFirstScreen: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool.isRequired,
}
