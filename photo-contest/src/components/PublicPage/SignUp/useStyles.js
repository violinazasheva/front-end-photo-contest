import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      align: 'center'
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: "#FFBF00",
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
      alignItems: 'center',
      align: 'center'
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
}));

export default useStyles;
