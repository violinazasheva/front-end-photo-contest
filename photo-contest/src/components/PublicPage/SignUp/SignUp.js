import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Swal from 'sweetalert2';
import useStyles from './useStyles';
// import Loader from '../../Loader/Loader';
import BASE_URL from '../../../common/constants';
import NavContext from '../../../providers/NavContext';
import AuthContext from '../../../providers/AuthContext';
// import UploadAvatar from './UploadAvatar';

const SignUp = ({ history }) => {
  const classes = useStyles();

  // Navbar - not transparent & sticky
  const { isActive, setActiveState } = useContext(NavContext);
  const { isLoggedIn, setLoginState, user } = useContext(AuthContext);

  setActiveState(false);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordCheck, setPasswordCheck] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');

  useEffect(() => {

    if (isLoggedIn) {

      if (user.role === 'Junkie') {
        history.push('/junkies');
      }

      if (user.role === 'Organizator') {
        history.push('/organizers');
      }
    }
  }, [history, isLoggedIn]);

  const handleUsername = event => {
    if (!event) {
      throw Error
    }
    setUsername(event.target.value)
  };

  const handlePassword = event => {
    if (!event) {
      throw Error
    }
    setPassword(event.target.value)
  }

  const handlePasswordCheck = event => {
    if (!event) {
      throw Error
    }
    setPasswordCheck(event.target.value)
  }

  const handleFirstName = event => {
    if (!event) {
      throw Error
    }
    setFirstName(event.target.value)
  };

  const handleLastName = event => {
    if (!event) {
      throw Error
    }
    setLastName(event.target.value)
  };

  //   if (loading) {
  //     return <Loader />
  //   }

  const handleSignUp = (ev) => {
    ev.preventDefault();

    if (password.length < 3 || password.length > 25 || username.length < 3 || username.length > 25) {
      return Swal.fire({
        icon: 'error',
        text: `Username and password should be between 3 and 25 symbols each!`,
      })
    }

    if (password !== passwordCheck) {
      return Swal.fire({
        icon: 'error',
        text: `Password inputs are not matching!`,
      })
    }
    if (firstName.length < 3 || firstName.length > 25 || 
      lastName.length < 3 || lastName.length > 25) {
      return Swal.fire({
        icon: 'error',
        text: `First and last name should be between 3 and 25 symbols!`,
      })
    }
    // setLoading(true);

    fetch(`${BASE_URL}/auth/signup`, {
      method: "POST",
      body: JSON.stringify({
        username: username,
        password: password,
        firstName: firstName,
        lastName: lastName
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message) {
          // alert("wrong")
          throw Error
        }
        setPasswordCheck('')
        setPassword('')
        setUsername('')
        setFirstName('')
        setLastName('')
        Swal.fire({
          icon: 'success',
          title: 'Congrats!',
          text: `Your account was created successfully! Please, login!`,
        })
        history.push('/signin');
      })
      .catch((error) => Swal.fire({
        icon: 'error',
        title: 'Sorry...',
        text: `User name is existing. Try login.`,
      })
      )
    // .finally(() => setLoading(false));
  }

  const clickSignUp = () => {
    history.push('/signin');
  }


  return (
    <main>
      <br />
      <br />
      <br />
      <br />
      <br />

      <Container component="main" maxWidth="xs" className={classes.root}>
        <CssBaseline />
        <Typography component="h1" align="center" variant="h5">
          <div align="center" justifyContent="center">
            <Avatar className={classes.avatar} align="center" justifyContent="center">
              <LockOutlinedIcon align="center" />
            </Avatar>
          </div>
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                onChange={handleUsername}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handlePassword}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handlePasswordCheck}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="firstName"
                label="First name"
                type="firstName"
                id="firstName"
                autoComplete="firstName"
                onChange={handleFirstName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="lastName"
                label="Last name"
                type="lastName"
                id="lastName"
                autoComplete="lastName"
                onChange={handleLastName}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            style={{ backgroundColor: "#FFBF00" }}
            className={classes.submit}
            onClick={handleSignUp}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" variant="body2" onClick={clickSignUp}>
                Already have an account? Sign in.
              </Link>
            </Grid>
          </Grid>
        </form>
      </Container>
    </main>
  )
}

SignUp.propTypes = {
  history: PropTypes.object.isRequired,
}

export default SignUp;
