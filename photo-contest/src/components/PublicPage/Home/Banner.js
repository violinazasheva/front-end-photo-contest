import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import ScrollElement from 'rc-scroll-anim/lib/ScrollElement';
import Icon from '@ant-design/icons';
import QueueAnim from 'rc-queue-anim';
import './style/Banner.css';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';
import Button from '@material-ui/core/Button';


const Banner = ({ history }) => {
  const { isLoggedIn, setLoginState, user } = useContext(AuthContext);

  const handleSignUpButton = () => {
    history.push('/signup');
  };

  const handleDashboardButton = () => {
    if (user.role === 'Junkie') {
      history.push('/junkies');
    }

    if (user.role === 'Organizator') {
      history.push('/organizers');
    }
  }

  const typeFunc = (a) => {
    if (a.key === 'line') {
      return 'right';
    } else if (a.key === 'button') {
      return 'bottom';
    }
    return 'left';
  }
  return (
    <section className="page banner-wrapper">
      <ScrollElement
        className="page"
        id="banner"
        // onChange={({ mode }) => onEnterChange(mode)}
        playScale={0.9}
      >
        <QueueAnim className="banner-text-wrapper" type={typeFunc} delay={300} key="banner">
          <span className="line" key="line" />
          <h2 key="h2"><b>PHOTO CONTEST</b></h2>
          <p key="content"><b>SHARE YOUR BEST WITH US</b></p>
          <div key="button1" className="start-button clearfix">
            {isLoggedIn ? <Button variant="contained" style={{"background": "#FFBF01", "textTransform": "capitalize", "fontWeight": "bold"}} onClick={handleDashboardButton}> Go back to your dashboard </Button>
              : <Button variant="contained" style={{"background": "#FFBF01", "textTransform": "capitalize", "fontWeight": "bold"}} onClick={handleSignUpButton}> Create your account</Button>}
          </div>
        </QueueAnim>
        <Icon type="down" className="down" />
      </ScrollElement>
    </section>
  );
}

export default withRouter(Banner);

Banner.propTypes = {
  history: PropTypes.object.isRequired,
}
