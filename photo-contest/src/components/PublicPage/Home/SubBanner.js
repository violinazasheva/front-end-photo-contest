import React from 'react';
import QueueAnim from 'rc-queue-anim';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import { Row, Col } from 'antd';
import { subBanner } from './data';
import './style/Banner.css';

const children = subBanner.map((d, i) => (
  <Col span={8} className="col" key={i.toString()}>
    <QueueAnim
      type="bottom"
      className="content-wrapper home-hover"
    >
      <div key="image" className="image">{d.svg}</div>
      <h3 key="h3">{d.title}</h3>
      {d.content}
      {d.exp && <div className="exp" key="exp">{d.exp}</div>}
    </QueueAnim>
  </Col>
));

const SubBanner = () => {
  
  return (
    <div className="home-layout-wrapper home-serve-wrapper">
      <OverPack className="home-layout" playScale={0.4}>
        <QueueAnim className="home-serve" type="bottom" key="home-func" ease="easeOutQuart" leaveReverse>
          <h2 key="h2">For the most impatient...</h2>
          <i key="i" className="line" />
          <QueueAnim
            key="content"
            component={Row}
            type="bottom"
            componentProps={{ gutter: 96 }}
          >
            {children}
          </QueueAnim>
        </QueueAnim>
      </OverPack>
    </div>
  )
}

export default SubBanner;
// export default function SubBanner() {
//   const children = subBanner.map((d, i) => (
//     <Col span={8} className="col" key={i.toString()}>
//       <QueueAnim
//         type="bottom"
//         className="content-wrapper home-hover"
//       >
//         <div key="image" className="image">{d.svg}</div>
//         <h3 key="h3">{d.title}</h3>
//         {d.content}
//         {d.exp && <div className="exp" key="exp">{d.exp}</div>}
//       </QueueAnim>
//     </Col>
//   ));
//   return (
//     <div className="home-layout-wrapper home-serve-wrapper">
//       <OverPack className="home-layout" playScale={0.4}>
//         <QueueAnim className="home-serve" type="bottom" key="home-func" ease="easeOutQuart" leaveReverse>
//           <h2 key="h2">For the most impatient...</h2>
//           <i key="i" className="line" />
//           <QueueAnim
//             key="content"
//             component={Row}
//             type="bottom"
//             componentProps={{ gutter: 96 }}
//           >
//             {children}
//           </QueueAnim>
//         </QueueAnim>
//       </OverPack>
//     </div>);
// }
