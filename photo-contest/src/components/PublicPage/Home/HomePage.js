import React, { useContext } from 'react';
import DocumentTitle from 'react-document-title';
// import { enquireScreen } from 'enquire-js';
import Banner from './Banner';
import SubBanner from './SubBanner';
import Footer from './../Footer/Footer';
import NavContext from './../../../providers/NavContext';

const HomePage = () => {

  // Navbar - transparent & flex
  const { isActive, setActiveState } = useContext(NavContext);
  setActiveState(true);

  return (
    <>
      <Banner key="banner" />
      <SubBanner key="subBanner" />
      <DocumentTitle title="PHOTO CONTEST" key="title" />
      <Footer key="footer" />
    </>
  );
}

export default HomePage;
