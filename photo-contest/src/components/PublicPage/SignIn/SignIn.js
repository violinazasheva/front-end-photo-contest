import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { Spin, Space } from 'antd';
import AuthContext, { extractUser } from '../../../providers/AuthContext';
import NavContext from '../../../providers/NavContext';
import BASE_URL from '../../../common/constants';
import useStyles from './Styles/useStyles';
import Swal from 'sweetalert2';

const SignIn = ({ history, location, match }) => {
  const classes = useStyles();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const { isLoggedIn, setLoginState, user } = useContext(AuthContext);

  // Navbar - not transparent & sticky
  const { isActive, setActiveState } = useContext(NavContext);
  setActiveState(false);

  useEffect(() => {

    if (isLoggedIn) {
  
      if (user.role === 'Junkie') {
        history.push('/junkies');
      }

      if (user.role === 'Organizator') {
        history.push('/organizers');
      }
    }
  }, [history, isLoggedIn]);

  const handleUsername = event => {
    setUsername(event.target.value)
  };

  const handlePassword = event => {
    setPassword(event.target.value)
  }

  const handleSubmit = () => {
    setLoading(true);

    fetch(`${BASE_URL}/auth/signin`, {
      method: "POST",
      body: JSON.stringify({
        username,
        password
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then(({ token }) => {
        if (!token) {
          throw new Error('Invalid username/password');
        }

        localStorage.setItem('token', token);

        setLoginState({
          isLoggedIn: !!extractUser(token),
          user: extractUser(token),
          token: token
        });
      })
      .catch((error) => Swal.fire({
        icon: 'error',
        text: `Wrong username or password!`,
      }))
      .finally(() => setLoading(false));
  }

  if (loading) {
    return (
      <Space size="middle">
        <Spin size="large" />
      </Space>
    )
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <br /><br /><br /><br /><br />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              onChange={handleUsername}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={handlePassword}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              style={{ backgroundColor: "#FFBF00" }}
              className={classes.submit}
              onClick={handleSubmit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/signup" variant="body2">
                  Don't have an account? Sign Up
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
  );
};

SignIn.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default SignIn;
