import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Timeline } from '@material-ui/lab';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import AssistantPhotoIcon from '@material-ui/icons/AssistantPhoto';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: '6px 16px',
    },
    secondaryTail: {
        backgroundColor: theme.palette.secondary.main,
    },
    gridList: {
      width: 200,
    }
}));

const CustomizedTimeline = ({ score, rank, untilNextRank, leftPoints }) => {
  const classes = useStyles();

  return (
    <Timeline align="alternate">
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot color="primary">
            <KeyboardArrowDownIcon />
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent className={classes.gridList}>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Junkie
            </Typography>
            <Typography>{rank === "Junkie" ? `Current points: ${score}` : null}</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineOppositeContent>
          <Typography variant="body2" color="textSecondary">
            {rank === "Junkie" ? `Points left until next rank: ${leftPoints}` : null}
          </Typography>
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineDot color="primary">
            <KeyboardArrowDownIcon />
          </TimelineDot>
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Enthusiast
            </Typography>
            <Typography>{rank === "Enthusiast" ? `Current points: ${score}` : null}</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineOppositeContent>
          <Typography variant="body2" color="textSecondary">
            {rank === "Enthusiast" ? `Points left until next rank: ${leftPoints}` : null}
          </Typography>
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineDot color="primary">
            <KeyboardArrowDownIcon />
          </TimelineDot>
          <TimelineConnector className={classes.secondaryTail} />
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Master
            </Typography>
            <Typography>{rank === "Master" ? `Current points: ${score}` : null}</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
      <TimelineItem>
        <TimelineOppositeContent>
          <Typography variant="body2" color="textSecondary">
            {rank === "Master" ? `Points left until next rank: ${leftPoints}` : null}
          </Typography>
        </TimelineOppositeContent>
        <TimelineSeparator>
          <TimelineDot color="secondary">
            <AssistantPhotoIcon />
          </TimelineDot>
        </TimelineSeparator>
        <TimelineContent>
          <Paper elevation={3} className={classes.paper}>
            <Typography variant="h6" component="h1">
              Wise and Benevolent Photo Dictator
            </Typography>
            <Typography>{rank === "Wise and Benevolent Photo Dictator" ? `Current points: ${score}` : null}</Typography>
          </Paper>
        </TimelineContent>
      </TimelineItem>
    </Timeline>
  );
}

CustomizedTimeline.propTypes = {
    score: PropTypes.number.isRequired,
    rank: PropTypes.string.isRequired,
    untilNextRank: PropTypes.number.isRequired,
    leftPoints: PropTypes.number.isRequired
}

export default CustomizedTimeline;
