import { makeStyles } from '@material-ui/core/styles';

const useStylesMultiSelect = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(0),
      minWidth: 395,
      maxWidth: 395,
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
}));

export default useStylesMultiSelect;
