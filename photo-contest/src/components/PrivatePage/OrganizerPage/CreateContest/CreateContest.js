import React, { useState, useEffect, useContext, useRef } from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Chip from '@material-ui/core/Chip';
import Box from '@material-ui/core/Box';
import Swal from 'sweetalert2';
import useStyles from '../Styles/useStyles';
import useStylesMultiSelect from './Styles/MultiSelectStyle';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import MenuProps from './Styles/MenuProps';
import BASE_URL from '../../../../common/constants';
import AuthContext from '../../../../providers/AuthContext';
import Form from 'react-bootstrap/Form';
import { withRouter } from 'react-router-dom';

const CreateContest = ({ history }) => {
  const classes = useStyles();
  const classes2 = useStylesMultiSelect();
  const inputRef = useRef();

  const { token } = useContext(AuthContext);

  const dateNow = new Date().toISOString();
  const currDateFormat = dateNow.slice(0, 10);

  const timeNow = new Date().toISOString();
  const currTimeFormat = timeNow.slice(0, 16);

  const [title, setTitle] = useState('');
  const [contestCover, setContestCover] = useState('');
  const [category, setCategory] = useState('');
  const [typeId, setTypeId] = useState('');
  const [ph1Limit, setPh1Limit] = useState('');
  const [ph2Limit, setPh2Limit] = useState('');
  const [jury, setJury] = useState([]);
  const [invitedUsers, setInvitedUsers] = useState([]);

  const [allUsers, setAllUsers] = useState([]);
  const [masterUsers, setMasterUsers] = useState([]);

  const handleTitle = (event) => {
    const titleInput = event.target.value;
    setTitle(titleInput);
  }

  const handleCategory = (event) => {
    const categoryInput = event.target.value;
    setCategory(categoryInput);
  }

  const handleTypeId = (event) => {
    const typeInput = event.target.value;
    setTypeId(typeInput);
  };

  const handleDateChange = (event) => {
    const dateInput = event.target.value;

    // checks if date is in range [1, 31] days
    const startDate = new Date(currDateFormat);
    const endDate = new Date(dateInput); 
    const differenceInTime = endDate.getTime() - startDate.getTime(); 
    const differenceInDays = differenceInTime / (1000 * 3600 * 24);

    if (differenceInDays >= 0 && differenceInDays < 32) {
      setPh1Limit(dateInput);
    } else {
      Swal.fire({
        icon: 'error',
        text: 'End date of Phase I should not be more than 31days after the start date of Phase I!',
      })
    }
  };

  const handleTimeChange = (event) => {
    const timeInput = event.target.value;

    const startDate = new Date(ph1Limit);
    const endDate = new Date(timeInput); 
    const differenceInTime = endDate.getTime() - startDate.getTime(); 
    const differenceInDays = Math.floor(differenceInTime / 1000 / 60 / 60);
    
    if (differenceInDays >= 0 && differenceInDays < 25) {
      setPh2Limit(timeInput);
    } else {
      Swal.fire({
        icon: 'error',
        text: 'End time of Phase II should not be more than 24h after the end of Phase I!',
      })
    }
  }

  const handleSelectJury = (event) => {
    setJury(event.target.value);
  }

  const handleInviteUsers = (event) => {
    setInvitedUsers(event.target.value);
  }

  const loadAllUsers = () => {
    fetch(`${BASE_URL}/users/all`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      setAllUsers(data);
    })
  }

  const loadMasterUsers = () => {
    fetch(`${BASE_URL}/users/withMasterRank`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      setMasterUsers(data);
    })
  }

  useEffect(() => {
    loadMasterUsers();
    loadAllUsers();
  }, []);

  const handleSubmit = () => {

    const contestBody = new FormData();
    contestBody.set('uploadPhoto', contestCover);
    contestBody.set('title', title);
    contestBody.set('category', category);
    contestBody.set('typeId', typeId);
    contestBody.set('ph1LimitDays', ph1Limit);
    contestBody.set('ph2LimitHours', ph2Limit);
    contestBody.set('selectJury', jury);
    contestBody.set('invitedUsers', invitedUsers);

    if (!title || !category || !typeId || !ph1Limit || !ph2Limit) {
      Swal.fire({
        icon: 'error',
        text: 'Fields with * are required!',
      })
      return;
    }

    fetch(`${BASE_URL}/contests/setup`, {
      method: "POST",
      body: contestBody,
      headers: {
        // 'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.message) {
        Swal.fire({
          icon: 'success',
          title: data.message,
        })
        history.push('/contests/phase/1');
      } else if (data.error) {
        Swal.fire({
          icon: 'error',
          title: data.error,
        })
      }
    })
  } 

  return (
    <>
      <br />
      <br />
      <CssBaseline />
      <main style={{ marginLeft: "160px" }}>
        <Container className={classes.cardGrid} maxWidth="xs">
          <Typography variant="h6" gutterBottom>
            Contest Details
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                required
                id="title"
                name="title"
                label="Title"
                onChange={handleTitle}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                id="category"
                name="category"
                label="Category"
                onChange={handleCategory}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.formControl}>
                <InputLabel id="select-type">Type</InputLabel>
                <Select
                  labelId="select-type"
                  id="select-type"
                    // value={type}
                  onChange={handleTypeId}
                >
                  <MenuItem value={1}>open</MenuItem>
                  <MenuItem value={2}>invitational</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <form className={classes.containerDate} noValidate>
                <TextField
                  id="date"
                  label="Phase I end date"
                  type="date"
                  defaultValue={currDateFormat}
                  className={classes.textFieldDate}
                  onChange={handleDateChange}
                  InputLabelProps={{
                      shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item xs={12}>
              <form className={classes.containerTime} noValidate>
                <TextField
                  id="datetime-local"
                  label="Phase II end time"
                  type="datetime-local"
                  defaultValue={currTimeFormat}
                  className={classes.textFieldTime}
                  onChange={handleTimeChange}
                  InputLabelProps={{
                     shrink: true,
                  }}
                />
              </form>
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes2.formControl}>
                <InputLabel id="select-jury">Select additional jury</InputLabel>       
                <Select
                  labelId="select-jury"
                  id="select-jury"
                  multiple
                  value={jury}
                  onChange={handleSelectJury}
                  input={<Input id="select-jury" />}
                  renderValue={(selected) => (
                    <div className={classes2.chips}>
                      {selected.map((value) => (
                        <Chip key={value} label={value} className={classes2.chip} />
                        ))}
                    </div>
                  )}
                  MenuProps={MenuProps}
                >
                  {masterUsers.map((name) => (
                    <MenuItem key={name} value={name}>
                      <Checkbox style={{ color: "#FFBF00" }} checked={jury.indexOf(name) > -1} />
                      <ListItemText primary={name} />
                    </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            {(typeId === 2) ?
              <Grid item xs={12}>
                <FormControl className={classes2.formControl}>
                  <InputLabel id="invite-users">Invite Users*</InputLabel>
                  <Select
                    labelId="invite-users"
                    id="invite-users"
                    multiple
                    value={invitedUsers}
                    onChange={handleInviteUsers}
                    input={<Input id="invite-users" />}
                    renderValue={(selected) => (
                      <div className={classes2.chips}>
                        {selected.map((value) => (
                          <Chip key={value} label={value} className={classes2.chip} />
                        ))}
                      </div>
                    )}
                    MenuProps={MenuProps}
                  >
                    {allUsers.map((name) => (
                      <MenuItem key={name} value={name}>
                        <Checkbox style={{ color: "#FFBF00" }} checked={invitedUsers.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid> : null}
            <Grid item xs={12}>                
              <Box fontSize="body1.fontSize" marginTop="30px">Choose cover photo
                <Form style={{marginTop: '30px', marginBottom: '30px'}}>
                  <Form.File 
                    id="custom-file"
                    ref={inputRef}
                    onChange={() => setContestCover(inputRef.current.files[0])}
                  />
                </Form>
              </Box>
            </Grid>
            <Button
              type="submit"
                // fullWidth
              variant="contained"
              style={{ backgroundColor: "#FFBF00", paddingLeft: "50px", paddingRight: "50px", marginLeft: "250px" }}
              className={classes.submit}
              onClick={handleSubmit}
            >
              CREATE
            </Button>
          </Grid>
        </Container>
      </main>
    </>
  )
}

CreateContest.propTypes = {
  history: PropTypes.object.isRequired
}
export default withRouter(CreateContest);

