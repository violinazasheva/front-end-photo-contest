import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import BASE_URL from '../../../../common/constants';
// import Loader from '../../Loader/Loader';
import { Container, Grid, Typography } from '@material-ui/core';
import AuthContext from '../../../../providers/AuthContext';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import junkiesStyle from '../Styles/junkiesStyle';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Button from '@material-ui/core/Button';
import NavContext from '../../../../providers/NavContext';
import Swal from 'sweetalert2';

const ViewJunkiesByRanks = ({ history }) => {
  const { token } = useContext(AuthContext);
  const classes = junkiesStyle();

  const [allJunkies, setallJunkies] = useState([]);
//   const [loading, setLoading] = useState(false);

// const { isActive, setActiveState } = useContext(NavContext);
// setActiveState(false);

const handleHomeButton = () => history.push('/home')

  useEffect(() => {
    // setLoading(true);

    fetch(`${BASE_URL}/users/descRanking`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        setallJunkies(data)
      })
      .catch((error) => Swal.fire({
        icon: 'error',
        title: 'Sorry...',
        text: `The users are not found.`,
      }))
    //   .finally(() => setLoading(false));
  }, []);

//   if (loading) {
//     return <Loader />
//   }

  const transformedJunkies = allJunkies.map((junkie) => {
    return (
      <>
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Avatar" src={`${BASE_URL}/public/${junkie.avatar}`} />
          </ListItemAvatar>
          <ListItemText
            primary={`${junkie.username} (${junkie.first_name} ${junkie.last_name})`}
            secondary={`Rank: ${junkie.rank}`}
          />
          <ListItemSecondaryAction>
            <ListItemText secondary="Points:" />
            <ListItemText secondary={junkie.curr_points} />
          </ListItemSecondaryAction>
        </ListItem>

        <Divider variant="inset" component="li" />
      </>
    );
  })
  return (
    <main>
      <br /><br /><br /><br /><br />
      <Container maxWidth="md">
        <Typography variant="h4" align="center" color="textSecondary" paragraph>
          All junkies ordered by rank
        </Typography>
        <List className={classes.root}>
          {transformedJunkies}
        </List>
        <br /><br />
        <Grid container justify="center">
          <Button variant="contained" style={{"background": "#FFBF01", "textTransform": "capitalize", "fontWeight": "bold"}} onClick={(e) => {e.preventDefault(); handleHomeButton()}}> Go back to home page </Button>
        </Grid>
      </Container>
      <br /><br />
    </main>
    )
}

ViewJunkiesByRanks.propTypes = {
  history: PropTypes.object.isRequired,
}

export default ViewJunkiesByRanks;

