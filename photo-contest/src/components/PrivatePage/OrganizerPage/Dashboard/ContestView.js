import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
// import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
// import { Button } from 'antd';


import { UserOutlined, TagsOutlined, FieldTimeOutlined } from '@ant-design/icons';

import useStyles from '../Styles/useStyles';
import BASE_URL from '../../../../common/constants';
import { withRouter } from 'react-router-dom';


const ContestView = ({ history, contestId, title, category, type, phase, username, ph1Days, ph2Hours, imageUrl }) => {

  const classes = useStyles();

  const handleClickContest = () => {
    history.push(`/contests/${contestId}/photos/${title}`);
  };

  return (
    <Grid item key={contestId} xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        <CardActionArea key={contestId} onClick={handleClickContest}>
          <CardMedia
            className={classes.cardMedia}
            image={`${BASE_URL}/public/${imageUrl}`}
            title="Image title"
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Box fontSize="h6.fontSize" color="primary.main">
              <TagsOutlined /> {category}
            </Box>
            <Box fontSize="body1.fontSize" color="primary.light">
              <UserOutlined /> {username}
            </Box>
            <br />
            {(type === 'open') ?
              <Box fontSize="body1.fontSize" color="success.main">
                {type}
              </Box> :
              <Box fontSize="body1.fontSize" color="warning.main">
                {type}
              </Box>}
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )   
}

ContestView.propTypes = {
  history: PropTypes.object.isRequired,
  contestId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  phase: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  ph1Days: PropTypes.string.isRequired,
  ph2Hours: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired
}

export default withRouter(ContestView);
