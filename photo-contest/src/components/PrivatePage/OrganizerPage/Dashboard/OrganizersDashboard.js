import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ContestView from './ContestView';
import AuthContext from '../../../../providers/AuthContext';
import BASE_URL from '../../../../common/constants';
import NavContext from '../../../../providers/NavContext';
import Swal from 'sweetalert2';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from '../Styles/useStyles';


const OrganizersDashboard = (props) => {
  const { user, token } = useContext(AuthContext);
  const [contestData, setContestData] = useState([]);
  const classes = useStyles();

  // const { isActive, setActiveState } = useContext(NavContext);
  // setActiveState(false);

  useEffect(() => {
    fetch(`${BASE_URL}/contests/phase/2`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      // if (data.length === 0) {
      //   throw Error
      // }
      setContestData(data);
    })
    .catch((err) => Swal.fire({
      title: 'Sorry...',
      text: `No contests in Phase 2`,
    }))

  }, []);
  const transformedContests = contestData.map((contest) => {
    return <ContestView key={contest.contestId} {...contest} />
  });

  return (
    <>
      <>
        <br />
        <br />
        <CssBaseline />
        <main>
          {/* Hero unit */}
          <div className={classes.heroContent}>
            <Container maxWidth="sm">
              <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Dashboard
              </Typography>
              <Typography variant="h5" align="center" color="textSecondary" paragraph>
                Some contests in Phase II you may want to review
              </Typography>
            </Container>
          </div>
          {transformedContests.length === 0 ?
            <Container className={classes.cardGrid} maxWidth="md">
              <Typography variant="h5" align="center" color="textSecondary" paragraph>
                There are no contests in Phase II.
              </Typography>
            </Container> :
            <Container className={classes.cardGrid} maxWidth="md">
              <Grid container spacing={4}>
                {transformedContests}
              </Grid>
            </Container>}
        </main>
      </>
      
    </>
  )      
}

OrganizersDashboard.propTypes = {
  props: PropTypes.object.isRequired,
}

export default OrganizersDashboard;
