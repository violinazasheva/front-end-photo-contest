import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';

import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from '../Styles/useStyles';

import SiderBar from './SiderBar';
import AuthContext from '../../../../providers/AuthContext';
import BASE_URL from '../../../../common/constants';
import ContestView from './ContestView';

import { withRouter } from 'react-router-dom'
import PhaseStepper from '../../Photos/PhaseStepper/PhaseStepper';

const ContestsInPhase = (props) => {
    const { id } = props.match.params;
    const { user, token } = useContext(AuthContext);
    const [contestData, setContestData] = useState([]);
    const classes = useStyles();
    useEffect(() => {
        fetch(`${BASE_URL}/contests/phase/${id}`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            }
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.length === 0) {
              setContestData([]);
            } else {
              setContestData(data)
            }
            
        })
        .catch((err) => Swal.fire({
          // icon: 'error',
          title: 'Sorry...',
          text: err.message,
        }))
    }, [id]);

    const transformedContests = contestData.map((contest) => {
        return <ContestView key={contest.contestId} {...contest} />
    });
    
    return (
      <>
        <br />
        <br />
        <>
          <CssBaseline />
          <main>
            {/* Hero unit */}
            <div className={classes.heroContent}>
              <Container maxWidth="sm">
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                  Dashboard
                </Typography>
                <PhaseStepper ph1={0} currPh={+id} />
                {/* <Typography variant="h5" align="center" color="textSecondary" paragraph>
                  Contests in Phase {id}
                </Typography> */}
              </Container>
            </div>
            {transformedContests.length === 0 ?
              <Container className={classes.cardGrid} maxWidth="md">
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                  There are no contests in Phase {id}.
                </Typography>
              </Container> :
              <Container className={classes.cardGrid} maxWidth="md">
                <Grid container spacing={4}>
                  {transformedContests}
                </Grid>
              </Container>}
          </main>
        </>
      </>
    )
}

ContestsInPhase.propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
}
export default withRouter(ContestsInPhase);
