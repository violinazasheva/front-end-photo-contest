import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import "antd/dist/antd.css";
import { Layout, Menu, Switch, Typography } from 'antd';
import {
  UserOutlined,
  PlusCircleOutlined,
  StarOutlined,
  CheckCircleOutlined,
  TagsOutlined,
  HomeOutlined
} from '@ant-design/icons';
import { withRouter } from 'react-router-dom'
import AuthContext from '../../../../providers/AuthContext';

const { Sider } = Layout;
const { SubMenu } = Menu;

const SiderBar = ({ history, location }) => {

  const { user, token } = useContext(AuthContext);

  const [siderTheme, setSiderTheme] = useState({
    theme: 'dark',
    current: '0',
  });

  const changeTheme = value => {
    setSiderTheme({
      theme: value ? 'dark' : 'light',
    });
  };

  const handleClick = e => {
    setSiderTheme({
      current: e.key,
      theme: siderTheme.theme
    });
  };

  const handleHomeButton = () => {
    if (user.role === 'Organizator') {
      history.push('/organizers');
    } else if (user.role === 'Junkie') {
      history.push('/junkies');
    }
  };

  // const handleSetupContest = () => {
  //   history.push('/contests/setup');
  // };

  // const handlePhaseClick = (ev) => {
  //   const phaseId = +ev.key
  //   history.push(`/contests/phase/${phaseId}`);
  // };

  // const handleViewJunkies = () => {
  //   history.push('/users/descRanking');
  // }

  // const handleContestButton = (ev) => {
  //   const contestType = ev.key;
  //   history.push(`/contests/${contestType}`)
  // }

  // const handleDetailsButton = () => {
  //   history.push(`/junkies/details`);
  // }
  return (
    <>
      <Sider
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
        }}
        theme={siderTheme.theme}
      >
        <br />
        <br />
        <div className="logo" />
        <Menu
          theme={siderTheme.theme}
          onClick={handleClick}
          // style={{ width: 256 }}
          // defaultOpenKeys={['sub1']}
          selectedKeys={[siderTheme.current]}
          mode="inline"
        >
          <Menu.Item key="10" icon={<HomeOutlined />} onClick={handleHomeButton}>Home</Menu.Item>
          <br />
          <br />

          {user.role === 'Junkie'? (
            <>
              {/* <SubMenu key="sub1" icon={<StarOutlined />} title="Contests"> */}
              {/* <Menu.Item key="invitational" icon={<TagsOutlined />} onClick={handleContestButton}>My invitations</Menu.Item>
              <Menu.Item key="open" icon={<PlusCircleOutlined />} onClick={handleContestButton}>Open contests</Menu.Item>
              <Menu.Item key="current" icon={<PlusCircleOutlined />} onClick={handleContestButton}>Current contests</Menu.Item>
              <Menu.Item key="finished" icon={<CheckCircleOutlined />} onClick={handleContestButton}>Previous contests</Menu.Item> */}
              {/* </SubMenu> */}
              {/* <Menu.Item key="4" icon={<UserOutlined />} onClick={handleDetailsButton}>Details</Menu.Item> */}
            </>

          ): 
            <>
              <Menu.Item key="11" icon={<PlusCircleOutlined />} onClick={handleSetupContest}>Create new contest</Menu.Item>
              <SubMenu key="sub1" icon={<StarOutlined />} title="Contests">
                <Menu.Item key="1" onClick={handlePhaseClick}>Phase 1</Menu.Item>
                <Menu.Item key="2" onClick={handlePhaseClick}>Phase 2</Menu.Item>
                <Menu.Item key="3" onClick={handlePhaseClick}>Finished</Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<UserOutlined />} title="Users">
                <Menu.Item key="13" onClick={handleViewJunkies}>Ordered by ranking</Menu.Item>
              </SubMenu>
            </>}
          


      
        </Menu>
        <br />
        <Switch
          checked={siderTheme.theme === 'dark'}
          onChange={changeTheme}
          checkedChildren="Dark"
          unCheckedChildren="Light"
        />
      </Sider>
    </>
  );
}

SiderBar.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object
}
export default withRouter(SiderBar);
