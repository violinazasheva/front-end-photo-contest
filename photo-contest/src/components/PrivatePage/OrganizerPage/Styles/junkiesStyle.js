import { makeStyles } from '@material-ui/core/styles';

const junkiesStyle = makeStyles((theme) => ({
    root: {
      width: '100%',
      maxWidth: '150ch',
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
  }));

export default junkiesStyle;
