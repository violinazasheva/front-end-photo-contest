import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  formControl: {
    margin: theme.spacing(0),
    width: 150,
  },
  containerDate: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textFieldDate: {
    marginLeft: theme.spacing(0),
    marginRight: theme.spacing(0),
    width: 150,
  },
  containerTime: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textFieldTime: {
    marginLeft: theme.spacing(0),
    marginRight: theme.spacing(0),
    width: 230,
  },
  formControlMultiSelect: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chipsMultiSelect: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chipMultiSelect: {
    margin: 2,
  },
  noLabelMultiSelect: {
    marginTop: theme.spacing(3),
  },
}));

export default useStyles;
