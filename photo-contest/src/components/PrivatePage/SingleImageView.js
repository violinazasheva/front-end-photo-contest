// import React, {useState} from 'react';
// import { Image } from 'antd';
// import BASE_URL from '../../common/constants';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import Modal from 'react-bootstrap/Modal';
// import Button from 'react-bootstrap/Button';
// import IconButton from '@material-ui/core/IconButton';
// import CommentIcon from '@material-ui/icons/Comment';
// import ViewReviews from '../PrivatePage/JunkiePage/ViewReviews';
// import GridListTile from '@material-ui/core/GridListTile';
// import GridListTileBar from '@material-ui/core/GridListTileBar';
// import PropTypes from 'prop-types';


// const SingleImageView = ({photo}) =>  {
//   const [show, setShow] = useState(false);
//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);

//     return(
//       <GridListTile key={photo.photo_id}>
//         <Image src={`${BASE_URL}/public/${photo.image_url}`} alt={photo.title} />
//         <GridListTileBar
//           title={photo.title} subtitle={<span>Story: {photo.story}</span>}
//           actionIcon={
//             <IconButton aria-label={`info about ${photo.title}`} className={classes.icon} onClick={(e) => {e.preventDefault(), handleShow()}}>
//               <CommentIcon label="review" />
//             </IconButton>
//   }
//         />
//         <Modal show={show} onHide={handleClose}>
//           <Modal.Header closeButton>
//             <Modal.Title>Reviews</Modal.Title>
//           </Modal.Header>
//           <Modal.Body><ViewReviews /></Modal.Body>
//           <Modal.Footer>
//             <Button variant="secondary" onClick={handleClose}>
//               Close
//             </Button>
//           </Modal.Footer>
//         </Modal>
//       </GridListTile>
//     )
// }

// SingleImageView.propTypes = {
//     photo: PropTypes.object.isRequired,
//   }

// export default SingleImageView;
