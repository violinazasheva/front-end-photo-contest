import React, { useState, useEffect, useContext } from 'react';
import './ChangeAvatar.scss'
import BASE_URL from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';
import { Tooltip } from 'antd';
import AvatarContext from '../../../providers/AvatarContext';

const ChangeAvatar = () => {
  const { token } = useContext(AuthContext);
  const { avatarName, setAvatarName } = useContext(AvatarContext);
  const [src, setSrc] = useState('');
  let inputElement;

  // displays the curr avatar
  useEffect(() => {
    fetch(`${BASE_URL}/users/avatar`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        setSrc(data.avatar)
      })
  }, [src]);

  function readURL(input) {
    if (input.files && input.files[0]) {
      let reader = new FileReader();
      reader.onload = function (e) {
        setSrc(e.target.result)
      }
      reader.readAsDataURL(input.files[0]);

      const formData = new FormData();
      formData.set('avatar', input.files[0]);

      // changing the avatar
      fetch(`${BASE_URL}/users/avatar`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData
      })
        .then(result => result.json())
        .then((data) => {
          setAvatarName(data.path);
        })
    }
  }
  function handleOnChange(e) {
    readURL(e.target);
  }

  function handleOnClick(e) {
    inputElement.click();
  }
  return (
    <div>
      <div className="avatar-wrapper">
        <img className="profile-pic" src={`${BASE_URL}/public/${src}`} alt="" />
        <div className="upload-button" onClick={handleOnClick}>
          <Tooltip placement="right" title="Change your avatar">
            <i className="fa fa-arrow-circle-up" aria-hidden="true" />
          </Tooltip>
        </div>
        <input className="file-upload" type="file" accept="image/*" onChange={handleOnChange} ref={input => inputElement = input} />
      </div>

    </div>
  );

}

export default ChangeAvatar;



