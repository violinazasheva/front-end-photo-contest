import React, {useContext, useState, useEffect} from 'react';
import './UserProfile.css';
import ImagesView from './ImagesView';
import BASE_URL from '../../common/constants';
import AuthContext from '../../providers/AuthContext';
import CustomizedTimeline from './TimeLine';
import ChangeAvatar from './ChangeAvatar/ChangeAvatar';

const UserProfile = () => {

  const [currPoints, setCurrPoints] = useState();
  const [currRank, setCurrRank] = useState();
  const [leftPoints, setLeftPoints] = useState();

  const { user, token } = useContext(AuthContext);

  if (user.role === 'Junkie') {
    useEffect(() => {  
      fetch(`${BASE_URL}/users/scoringInfo`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => response.json())
      .then((data) => {
        setCurrPoints(data.currPoints);
        setCurrRank(data.type);
      });
      fetch(`${BASE_URL}/users/leftPoints`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => response.json())
      .then((data) => {
        setLeftPoints(+data.pointsLeft);
      });
    }, [currPoints, leftPoints]);
  }

  return (
    <>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
      <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossOrigin="anonymous" />
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
      <link rel="stylesgeet" href="https://rawgit.com/creativetimofficial/material-kit/master/assets/css/material-kit.css" />

      <div className="main main-raised">
        <div className="profile-content">
          <div className="profile">
            <div className="avatar">
              <ChangeAvatar />
            </div>
            <div className="name">
              <h3 className="title">{user.firstName} {user.lastName}</h3>
              <h5>{user.role === "Junkie" ? `${currRank}` : user.role}</h5>
            </div>
          </div>
        </div>
        <br />
        {user.role === "Junkie" ? 
          <>
            <CustomizedTimeline score={+currPoints} rank={currRank} leftPoints={+leftPoints} /> 
            <div className="description text-center">
              <h4>Check out your contests history...</h4>
              <br />
              {user.role === "Junkie" ? <ImagesView /> : null}
            </div>
          </>
        : null}
      </div>
    </>
  );
};

export default UserProfile;
