import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import AuthContext from '../../../providers/AuthContext';
import PhotoDetails from './PhotoDetails';
import BASE_URL from '../../../common/constants';
import photosUseStyles from './Styles/photosUseStyles';
import PhaseStepper from './PhaseStepper/PhaseStepper';
import WinnerPhotoDetails from './WinnerPhotoDetails';

const AllPhotos = (props) => {
  const classes = photosUseStyles();
  const { id } = props.match.params;
  const { title } = props.match.params;
  
  const [photoData, setPhotoData] = useState([]);
  const [contestData, setContestData] = useState([]);
  const [isJury, setIsJury] = useState(false);
  const [winnerPhotosData, setWinnerPhotosData] = useState([]);
  
  const { user, token } = useContext(AuthContext);

  const getContestInfo = () => {
    fetch(`${BASE_URL}/contests/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.length === 0) {
        throw new Error (`Sorry, contest with id ${id} does not exist`);
      }
      setContestData(data.data);
      setIsJury(data.jury);
    })
    .catch((err) => alert(`Sorry, contest with id ${id} does not exist`) )
  };

  const dateNow = new Date().toISOString();
  const currDateFormat = dateNow.slice(0, 10);

  const timeNow = new Date().toISOString();
  const currTimeFormat = timeNow.slice(0, 16);

  const ph1Sum = (contestData) => {
    if (contestData.length > 0) {
      const ph1Time = contestData.map((con) => con.ph1LimitDays)
      const startDate = new Date(currDateFormat);
      const endDate = new Date(ph1Time); 
      const differenceInTime = endDate.getTime() - startDate.getTime(); 
      const differenceInDays = differenceInTime / (1000 * 3600 * 24);
      return differenceInDays;
    }
  };
  const ph1LeftTime = ph1Sum(contestData);
  const phaseId = contestData.map((contest) => contest.phaseId).join();

  const ph2Sum = (contestData) => {
    if (contestData.length > 0) {
      const ph2Time = contestData.map((con) => con.ph2LimitHours)
      const startDate = new Date(currTimeFormat);
      const endDate = new Date(ph2Time); 
      const differenceInTime = endDate.getTime() - startDate.getTime(); 
      const differenceInDays = Math.floor(differenceInTime / 1000 / 60 / 60);
      return differenceInDays;
    }
  };
  const ph2LeftTime = ph2Sum(contestData);
  
  useEffect(() => {
    getContestInfo();
    // checkIfJunkieIsJury();

    fetch(`${BASE_URL}/contests/${id}/photos`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.message) {
        throw new Error(data.message);
      }
      setPhotoData(data);
    })
    .catch((err) => alert(err.message));

    fetch(`${BASE_URL}/contests/${id}/winners`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      setWinnerPhotosData(data);
    })
    .catch((err) => alert(err.message));
  }, []);
  
  const transformedPhotos = photoData.map((photo) => {
    return <PhotoDetails key={photo.photoId} phaseId={+phaseId} isJury={isJury} {...photo} />
  })

  const transformedWinnerPhotos = winnerPhotosData.map((photo) => {
    return <WinnerPhotoDetails key={photo.photoId} {...photo} />
  })

  return (
    <>
      <CssBaseline />
      <main>
        <br />
        <br />
        <br />
        <div className={classes.heroContent}>
          <Container maxWidth="md">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              {title}
            </Typography>
            <PhaseStepper ph1={ph1LeftTime} ph2={ph2LeftTime} currPh={+phaseId} />
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="lg">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {transformedPhotos.length === 0 ?
              <Container className={classes.cardGrid} maxWidth="md">
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                  This contest has no photos.
                </Typography>
              </Container> : null}
            {(+phaseId === 3 && transformedPhotos.length > 0) ?                          
              <Container maxWidth="lg">
                <Typography component="h1" variant="h3" align="center" color="inherit" gutterBottom>
                  Winners
                </Typography>
                <br />
                {transformedWinnerPhotos}
              </Container>
            : null}
            <br />
            {(user.role === 'Junkie' && (+phaseId === 1) && !isJury) ? 
              <Typography variant="h4" align="center" color="textSecondary" paragraph>
                All photos with their score and reviews will be visible when contest finishes. 
                <br />
                Please, be patient!
              </Typography> :
              <Container maxWidth="lg">
                <br />
                <br />
                {transformedPhotos.length > 0 ? 
                  <Typography component="h1" variant="h3" align="center" color="inherit" gutterBottom>
                    All Photos
                  </Typography> : null}
                <br />
                {transformedPhotos}
              </Container>}
          </Grid>
        </Container>
      </main>      
    </>
  );
}

AllPhotos.propTypes = {
  match: PropTypes.object.isRequired
}
export default AllPhotos;
