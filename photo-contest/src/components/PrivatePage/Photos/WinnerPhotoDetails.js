import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Image } from 'antd';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import useStyles from '../JunkiePage/Styles/useStyles';
import Grid from 'antd/lib/card/Grid';
import BASE_URL from '../../../common/constants';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Button from 'react-bootstrap/Button';

const WinnerPhotoDetails = ({ photoId, username, avatar, title, story, avgScore, imageUrl }) => {
  const classes = useStyles();

  const [showStory, setShowStory] = useState(false);
  const [photoStoryId, setPhotoStoryId] = useState(0);

  const handleShowStory = () => {
    setShowStory(true);
    setPhotoStoryId(photoId);
  };

  const handleClose = () => {
    setShowStory(false);
    setPhotoStoryId(0);
  };

  return (
    <>
      <Grid key={photoId} xs={16} spacing={3}>
        <Card className={classes.card}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                <img id='avatar' src={`${BASE_URL}/public/${avatar}`} />
              </Avatar>
            }
            title={title}
            action={
              <div className={classes.storyButton}>
                <IconButton
                  edge='end'
                  aria-label="show-story"
                  onClick={handleShowStory}
                >
                  <InfoOutlinedIcon />
                </IconButton>
              </div>
            }
            subheader={`@${username}`}
            
          />
          <Image
            width={345}
            height={250}
            src={`${BASE_URL}/public/${imageUrl}`}
          />
          <CardContent>
            <div className={classes.totalScore} spacing>
              <Rating
                name="jury-rate"
                value={1}
                max={1}
                readOnly
              />
              <Box ml={1}><b>{avgScore.toFixed(1)}</b></Box>
            </div>
          </CardContent>
          {photoId === photoStoryId ?
            <div>
              <Modal isOpen={showStory} className='className'>
                <ModalHeader>Story</ModalHeader>
                <ModalBody>
                  {story}
                </ModalBody>
                <ModalFooter>
                  <Button color="secondary" onClick={handleClose}>Close</Button>
                </ModalFooter>
              </Modal>
            </div> : null}
        </Card>
      </Grid>
    </>
  )
}

WinnerPhotoDetails.propTypes = {
  photoId: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  story: PropTypes.string.isRequired,
  avgScore: PropTypes.number.isRequired,
  imageUrl: PropTypes.string.isRequired,
  
}
export default WinnerPhotoDetails;
