import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { Image } from 'antd';
import AuthContext from '../../../providers/AuthContext';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import RateReviewItem from '@material-ui/icons/RateReview';
import useStyles from '../JunkiePage/Styles/useStyles';
import Grid from 'antd/lib/card/Grid';
import BASE_URL from '../../../common/constants';
import CreateReviewField from './CreateReview';
import ViewReviews from '../../../components/PrivatePage/JunkiePage/ViewReviews';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const PhotoDetails = ({ phaseId, isJury, photoId, contestId, imageUrl, title, story, username, avatar, avgScore }) => {

  const classes = useStyles();

  const {user, token} = useContext(AuthContext);
  const [reviewData, setReviewData] = useState([]);
  const [hasReviewdPhoto, setHasReviewdPhoto] = useState(0);
  const [wrote, setWrote] = useState(false);
  const [showReviews, setShowReviews] = useState(false);
  const [photoModalId, setphotoModalId] = useState(0);
  const [showStory, setShowStory] = useState(false);
  const [photoStoryId, setPhotoStoryId] = useState(0);

  const handleClose = () => {
    setShowReviews(false);
    setphotoModalId(0);
    setShowStory(false);
    setPhotoStoryId(0);
  };

  const handleShowReviews = () => {
    setShowReviews(true);
    setphotoModalId(photoId);
  }

  const handleShowStory = () => {
    setShowStory(true);
    setPhotoStoryId(photoId);
  }
  useEffect(() => {
    setWrote(false);

    fetch(`${BASE_URL}/photos/${photoId}/allReviews`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
    .then((response) => response.json())
    .then((data) => {
      setHasReviewdPhoto(data);
    });

    if (reviewData.comment) {
      
      fetch(`${BASE_URL}/photos/${photoId}/review`, {
        method: "POST",
        body: JSON.stringify(reviewData),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
         throw new error(data.error);
        }
        setReviewData([]);
        setWrote(true);
      })
      .catch((err) => alert(err))
    }
  }, [reviewData, wrote]);

  const handleReviewButton = async () => CreateReviewField(setReviewData, imageUrl, contestId, token);

  return (
    <>
      <Grid key={photoId} xs={16} spacing={3}>
        <Card className={classes.card}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                <img id='avatar' src={`${BASE_URL}/public/${avatar}`} />
              </Avatar>
            }
            title={title}
            action={
              <div className={classes.storyButton}>
                <IconButton
                  edge='end'
                  aria-label="show-story"
                  onClick={handleShowStory}
                >
                  <InfoOutlinedIcon />
                </IconButton>
              </div>
            }
            subheader={`@${username}`}
          />
          <Image
            width={345}
            height={250}
            src={`${BASE_URL}/public/${imageUrl}`}
          />
          <CardContent>
            {/* <Typography variant="body2" color="textSecondary" component="p">
              {story}
            </Typography> */}
            {/* <br /> */}
            {phaseId !== 3 ? null :
            <div className={classes.totalScore}>
              <Rating
                name="rating-star"
                value={1}
                max={1}
                readOnly
              />
              <Box ml={1}><b>{avgScore.toFixed(1)}</b></Box>
            </div>}
          </CardContent>
          {(phaseId === 1) ?
            <>
              {photoId === photoStoryId ?
                <div>
                  <Modal isOpen={showStory} className='className'>
                    <ModalHeader>Story</ModalHeader>
                    <ModalBody>
                      {story}
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={handleClose}>Close</Button>
                    </ModalFooter>
                  </Modal>
                </div> : null}
            </> : 
            <>
              <CardActions disableSpacing>
                {(phaseId === 3 || (phaseId === 2 && (isJury || user.role === 'Organizator'))) ?
                  <IconButton 
                    aria-label="show-reviews"
                    onClick={handleShowReviews}
                  >
                    <RateReviewItem />
                  </IconButton> : null}
                {(hasReviewdPhoto.message === 'not reviewd' && +hasReviewdPhoto.photoId === photoId && phaseId === 2) && (isJury || user.role === 'Organizator') ?
                  <IconButton aria-label="write-review" onClick={handleReviewButton}>
                    <CreateIcon />
                  </IconButton> : null}
              </CardActions>
              {photoId === photoModalId ? 
                <div>
                  <Modal isOpen={showReviews} className='className'>
                    <ModalHeader>Reviews</ModalHeader>
                    <ModalBody>
                      <ViewReviews photoId={photoId} />
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={handleClose}>Close</Button>
                    </ModalFooter>
                  </Modal>
                </div> : null}
              {photoId === photoStoryId ?
                <div>
                  <Modal isOpen={showStory} className='className'>
                    <ModalHeader>Story</ModalHeader>
                    <ModalBody>
                      {story}
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={handleClose}>Close</Button>
                    </ModalFooter>
                  </Modal>
                </div> : null}
            </>}
        </Card>
      </Grid>
    </>
  )
}

PhotoDetails.propTypes = {
  phaseId: PropTypes.number.isRequired,
  isJury: PropTypes.bool.isRequired,
  photoId: PropTypes.number.isRequired,
  contestId: PropTypes.number.isRequired,
  imageUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  story: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  avgScore: PropTypes.number.isRequired,
}
export default PhotoDetails;
