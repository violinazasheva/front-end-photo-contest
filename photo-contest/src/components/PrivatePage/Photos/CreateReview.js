import Swal from 'sweetalert2';
import BASE_URL from '../../../common/constants';

const CreateReviewField = async (setReviewData, imageUrl, contestId, token) => {
    Swal.mixin({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1', '2', '3']
    }).queue([
      {
        // title: 'Step 1',
        text: 'Does this photo fit the category?',
        input: 'checkbox',
        inputValue: 1,
        inputPlaceholder: 'This photo fits the category',
        confirmButtonText: 'Continue',
        inputValidator: (value) => {
          if (!value) {
            Swal.fire({
              title: 'Are you sure this photo does not fit the category?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes'
            }).then((result) => {
              if (result.isConfirmed) {
                setReviewData({
                  score: 0,
                  comment: 'Wrong category',
                  checkbox: 0
                });
                Swal.fire(
                  'Thank you for reviewing the photo',
                )
              }
            })
          }
        }
      },
      {
        // title: 'Step 2',
        text: 'Share how you feel about this photo',
        input: 'textarea',
        inputPlaceholder: 'Write your review',
        inputValidator: (value) => {
          if (!value || value.length < 15) {
            Swal.fire({
              icon: 'error',
              text: 'Review should be more than 15 symbols!',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'OK'
            })    
          }
        }
      },
      {
        input: 'range',
        inputLabel: 'How much do you like this photo from 1 to 10?',
        inputAttributes: {
          min: 1,
          max: 10,
          step: 1
        },
        inputValue: 5
      }
    ]).then((result) => {
      if (result.value) {
        const answers = (result.value); 
        setReviewData({
          score: +answers[2],
          comment: answers[1],
          checkbox: answers[0]
        });
        
        Swal.fire({
          title: 'All done! Thank you for reviewing this photo!',
          imageUrl: `${BASE_URL}/public/${imageUrl}`,
          imageWidth: 200,
          imageHeight: 200,
          imageAlt: 'Custom image',
          confirmButtonText: 'Great!'
        })
      }
    })
}

export default CreateReviewField;
