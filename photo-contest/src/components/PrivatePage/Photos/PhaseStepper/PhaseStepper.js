import React from 'react';
import PropTypes from 'prop-types';
import { Steps } from 'antd';
const { Step } = Steps;

const PhaseStepper = ({ ph1, ph2, currPh}) => {

    return (
      <>
        {ph1 === 0 ? 
          <Steps 
            current={currPh - 1}
          >
            {currPh === 1 ?
              <>
                <Step 
                  title="Phase I"
                />
                <Step 
                  title="Phase II"
                />
                <Step 
                  title="Finished"
                />
              </> : null}
            {currPh === 2 ?
              <>
                <Step 
                  title="Phase I"
                />
                <Step 
                  title="Phase II"
                />
                <Step 
                  title="Finished"
                />
              </> : null}
            {currPh === 3 ?
              <>
                <Step 
                  title="Phase I"
                />
                <Step 
                  title="Phase II"
                />
                <Step 
                  title="Finished"
                />
              </> : null}
          </Steps> :
          <Steps 
            current={currPh - 1}
          >
            {currPh === 1 ?
              <>
                <Step 
                  title="Phase I" 
          // subTitle={`${ph1} days until Phase II`}
                  description={`${ph1} day(s) until Phase II`}
                />
                <Step 
                  title="Phase II"
                />
                <Step 
                  title="Finished" 
                  description="This is a description."
                />
              </> : null}
            {currPh === 2 ?
              <>
                <Step 
                  title="Phase I"
                />
                <Step 
                  title="Phase II"
                  description={`${ph2} hour(s) until Finish`}
                />
                <Step 
                  title="Finished"
                />
              </> : null}
            {currPh === 3 ?
              <>
                <Step 
                  title="Phase I"
                />
                <Step 
                  title="Phase II"
                />
                <Step 
                  title="Finished"
                />
              </> : null}
          </Steps>}
      </>
    )
}

PhaseStepper.propTypes = {
  ph1: PropTypes.number.isRequired,
  ph2: PropTypes.number.isRequired,
  currPh: PropTypes.number.isRequired
}
export default PhaseStepper;
