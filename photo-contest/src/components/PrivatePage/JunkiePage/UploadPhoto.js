import Swal from 'sweetalert2';
import BASE_URL from '../../../common/constants';

const UploadPhoto = async (token, contestId, setCanUpload) => {

  const { value: file } = await Swal.fire({
    title: 'Select image',
    input: 'file',
    inputAttributes: {
      'accept': 'image/*',
      'aria-label': 'Upload your profile picture'
    },
    html:
      '<input placeholder="Title" id="swal-input1" class="swal2-input">' +
      '<input placeholder="Story" id="swal-input2" class="swal2-input">',
    focusConfirm: false,
    showCancelButton: true,
  })

  const formData = new FormData();
  formData.set('title', document.getElementById('swal-input1').value);
  formData.set('story', document.getElementById('swal-input2').value);
  formData.set('uploadPhoto', file);

  if (formData) {
    if(!document.getElementById('swal-input1').value || !document.getElementById('swal-input2').value || !file){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'You should fill each input field!',
      })
    } else if (document.getElementById('swal-input1').value.length && (document.getElementById('swal-input1').value.length < 2 || document.getElementById('swal-input1').value.length > 50)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Title should be a short text in the range [2...50] symbols!',
      })
    } else if (document.getElementById('swal-input2').value.length && (document.getElementById('swal-input2').value.length < 15 || document.getElementById('swal-input2').value.length > 100)) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Story should be a long text in the range [15...100] symbols',
      })
    } else if (file && ((file.name.substr(file.name.length - 4)) !== '.jpg' && (file.name.substr(file.name.length - 4)) !== '.png')) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'File extension should be .jpg or .png!',
      })
    } else {
      fetch(`${BASE_URL}/contests/${contestId}/uploadPhoto`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      })

      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: data.error,
          });
        } else {
          setCanUpload(false);
          if (file) {
            const reader = new FileReader()
            reader.onload = (e) => {
              Swal.fire({
                title: 'Your uploaded picture',
                imageUrl: e.target.result,
                imageAlt: 'The uploaded picture'
              })
            }
            reader.readAsDataURL(file)
          }
        }
      })
    }
  }
};

export default UploadPhoto;
