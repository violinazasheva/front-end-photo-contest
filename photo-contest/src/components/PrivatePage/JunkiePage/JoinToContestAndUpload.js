import React, { useState, useEffect, useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import PropTypes from 'prop-types';
import BASE_URL from '../../../common/constants';
import UploadPhoto from './UploadPhoto.js';
import { Button } from 'antd';
import Swal from 'sweetalert2';

const JoinToContestAndUpload = ({ contestId, title }) => {
  const { user, token } = useContext(AuthContext);
  const [canBeJoined, setCanBeJoined] = useState();
  const [canUpload, setCanUpload] = useState();

  useEffect(() => {
    fetch(`${BASE_URL}/contests/${contestId}/joinavailable`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data === true) {
        setCanBeJoined(true)
      } else {
        setCanBeJoined(false)
      }
    });

    fetch(`${BASE_URL}/contests/${contestId}/uploadCheck`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data === true) {
        setCanUpload(true)
      } else {
        setCanUpload(false)
      }
    });

  }, []);

  const handeJoin = () => {
    fetch(`${BASE_URL}/contests/${contestId}`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
      .then((response) => response.json())
      .then((data) => {
        setCanBeJoined(false);
        setCanUpload(true);
      
        Swal.fire({
          title: `You have successfully joined to contest ${title}.`,
          showCancelButton: true,
          confirmButtonText: `Upload`,
          denyButtonText: `Cancel`,
        }).then((result) => {
          if (result.isConfirmed) {
            UploadPhoto(token, contestId, setCanUpload);
          }
        })
      })
  }

  return (
    <>
      {canBeJoined ?
        <Button type="primary" danger onClick={(e) => {handeJoin(); e.preventDefault();}}>Join</Button>
        : ( canUpload ? <Button type="primary" block onClick={(e) => {UploadPhoto(token, contestId, setCanUpload); e.preventDefault();}}>Upload</Button> : <Button type="text" danger> </Button>)}
    </>
  )
}

JoinToContestAndUpload.propTypes = {
  contestId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
}

export default JoinToContestAndUpload;

