import React, { useContext, useEffect, useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import JunkieContestView from './JunkieContestView';
import useStyles from './Styles/useStyles';
import BASE_URL from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';
import ContestView from '../OrganizerPage/Dashboard/ContestView';

const ViewInvitational = (props) => {
  const { user, token } = useContext(AuthContext);
  const [contestJoinData, setContestJoinData] = useState([]);
  const [contestJuryData, setContestJuryData] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    fetch(`${BASE_URL}/contests/invitational`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        }
    })
    .then((response) => response.json())
    .then((data) => {
        if (data.length > 0) {
            setContestJoinData(data);
        }
    });

    fetch(`${BASE_URL}/users/invitedAsJury`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.length > 0) {
        setContestJuryData(data);
      }
    });
  }, []);

  const transformedContestsToJoin = contestJoinData.map((contest) => {
    return <JunkieContestView key={contest.contestId} {...contest} />
  });

  const transformedJuryContests = contestJuryData.map((contest) => {
    return <ContestView key={contest.contestId} {...contest} />
  });

  return (
    <>
      <br />
      <br />
      <>
        <CssBaseline />
        <main>
          <Container className={classes.cardGrid} maxWidth="md">
            <Typography variant="h4" align="center" color="textSecondary" paragraph>
              Invitational contests
            </Typography>
            {contestJoinData.length > 0 ? (
              <Grid container spacing={4}> {transformedContestsToJoin} </Grid>)
            : (
              <Typography variant="h8" align="center" color="textSecondary" paragraph>
                There are no contests that you are invited in.
              </Typography>)}
          </Container>
          <Container className={classes.cardGrid} maxWidth="md">
            <Typography variant="h4" align="center" color="textSecondary" paragraph>
              Contests you can review
            </Typography>
            {contestJuryData.length > 0 ? (
              <Grid container spacing={4}> {transformedJuryContests} </Grid>)
            : (
              <Typography variant="h8" align="center" color="textSecondary" paragraph>
                There are no contests that you are invited as jury.
              </Typography>)}
          </Container>
        </main>
      </>
    </>
  );
}

export default ViewInvitational;

