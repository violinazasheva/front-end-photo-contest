import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { UserOutlined, TagsOutlined } from '@ant-design/icons';
import Swal from 'sweetalert2';
import useStyles from './Styles/useStyles';
import BASE_URL from '../../../common/constants'
import { withRouter } from 'react-router-dom';
import JoinToContestAndUpload from './JoinToContestAndUpload';

const JunkieContestView = ({ history, contestId, title, category, phase, type, ph1LimitDays, ph2LimitHours, creator, imageUrl }) => {

  const classes = useStyles();
  const handleClickContest = () => {
    if (phase === 'Phase I') {
      Swal.fire({
        icon: 'info',
        title: 'Photos will be visible when Phase I finishes.'
      })
    } else {
      history.push(`/contests/${contestId}/photos/${title}`);
    }
  };

  return (
    <Grid item key={contestId} xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        <CardActionArea key={contestId} onClick={handleClickContest}>
          <CardMedia
            className={classes.cardMedia}
            image={`${BASE_URL}/public/${imageUrl}`}
            title="Image title"
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Box fontSize="h6.fontSize" color="primary.main">
              <TagsOutlined /> {category}
            </Box>
            <Box fontSize="body1.fontSize" color="primary.light">
              <UserOutlined /> {creator}
            </Box>
          </CardContent>
        </CardActionArea>
        {phase === 'Phase I' ?
          <JoinToContestAndUpload contestId={contestId} title={title} /> : null}
      </Card>
    </Grid>
  )   
}

JunkieContestView.propTypes = {
    history: PropTypes.object,
    contestId: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    phase: PropTypes.string.isRequired,
    creator: PropTypes.string.isRequired,
    ph1LimitDays: PropTypes.string.isRequired,
    ph2LimitHours: PropTypes.string.isRequired,
    creator: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired
}

export default withRouter(JunkieContestView);
