import React, { useState, useContext, useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from '../Styles/useStyles';
import BASE_URL from '../../../../common/constants';
import AuthContext from '../../../../providers/AuthContext';

const JunkieDetails = () => {
  const classes = useStyles();
  const { token } = useContext(AuthContext);
  const bull = <span className={classes.bullet}>•</span>;
  const [info, setInfo] = useState('');
  useEffect(() => {
    fetch(`${BASE_URL}/users/scoringInfo`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
        setInfo(data);
    })

  }, []);

  return (
    <>
      <CssBaseline />
      <main style={{ marginLeft: "160px" }}>
        <Container className={classes.cardGrid} maxWidth="md">
          <Typography variant="h4" align="center" color="textSecondary" paragraph>
            Details
          </Typography>
          <Grid item xs={6}>
            <Card className={classes.root}>
              <CardContent>
                <Box fontSize="h6.fontSize" color="warning.main">
                  @{info.username}
                </Box>
                <br />
                <Box fontSize="h5.fontSize" color="text.primary">
                  {bull} {info.type}
                </Box>
                <br />
                <Box fontSize="body1.fontSize" color="text.primary">
                  current points: {info.currPoints}
                </Box>
                <Box fontSize="body1.fontSize" color="text.primary">
                  needed points to reach next rank: {info.leftPoints}
                </Box>
              </CardContent>
            </Card>
          </Grid>
        </Container>
      </main>
    </>
  )
}

export default JunkieDetails;
