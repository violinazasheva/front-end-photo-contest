import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import JunkieContestView from './JunkieContestView';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from './Styles/useStyles';
import BASE_URL from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';

const ViewCurrent = () => {
  const { token } = useContext(AuthContext);
  const [contestCurrentData, setContestCurrentData] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    fetch(`${BASE_URL}/contests/current`, {
      headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.length > 0) {
        setContestCurrentData(data);
      }
    })
  }, []);

  const transformedCurrentContests = contestCurrentData.map((contest) => {
    return <JunkieContestView key={contest.contestId} {...contest} />
  });

  return (
    <>
      <br />
      <br />
      <Container className={classes.cardGrid} maxWidth="md">
        <Typography variant="h4" align="center" color="textSecondary" paragraph>
          Contests you are currently participating in
        </Typography>
        {contestCurrentData.length > 0 ? <Grid container spacing={4}> {transformedCurrentContests} </Grid>
      : (
        <Typography variant="h6" align="center" color="textSecondary" paragraph>
          There are no current contests.
        </Typography>)}
      </Container>
    </>
  );
}

export default ViewCurrent;
