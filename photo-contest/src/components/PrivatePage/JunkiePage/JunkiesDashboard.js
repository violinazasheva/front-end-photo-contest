import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from './Styles/useStyles';
import ViewOpen from './ViewOpen';
import { withRouter } from 'react-router-dom';

const JunkiesDashboard = () => {
  const classes = useStyles();

  return (
    <>
      <CssBaseline />
      <main>
        <br />
        <br />
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              Dashboard
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Share your best photos with us!
            </Typography>
          </Container>
        </div>
        <ViewOpen />
      </main>
    </> 
  )
}

export default withRouter(JunkiesDashboard);
