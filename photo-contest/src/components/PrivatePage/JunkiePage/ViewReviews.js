import React, { useState, useEffect, useContext} from 'react';
import { Comment,  Avatar } from 'antd';
import PropTypes from 'prop-types';
import BASE_URL from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import useStyles from './Styles/useStyles';

const ViewReviews = ({photoId}) => {
  const { token } = useContext(AuthContext);
  const classes = useStyles();
  const [allReviews, setAllReviews] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}/photos/${photoId}/reviews`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response.json())
    .then(data => {
      setAllReviews(data)
    })
    .catch((err) => alert(err.message));
  }, [photoId]);

  const transformedReviews = allReviews.map((review) => {
    return(
      <Comment 
        key={review.id}
        author={<p>{review.first_name} {review.last_name}</p>}
        avatar={
          <Avatar
            src={`${BASE_URL}/public/${review.avatar}`}
            alt={review.username}
          />
        }
        content={
          <p>
            {review.comment}
            <br />
            <br />
            <div className={classes.reviews}>
              <Rating
                name="jury-rate"
                value={1}
                max={1}
                readOnly
              />
              <Box ml={1}><b>{review.score}</b></Box>
            </div>
          </p>
        }
        datetime={
          <p>
            @{review.username}
          </p>
        }
      />
    )
  })

  return (
    <>
      {transformedReviews.length !== 0 ? transformedReviews : <div>No reviews</div>}
    </>
  );
};

ViewReviews.propTypes = {
  photoId: PropTypes.number.isRequired,
}

export default ViewReviews;
