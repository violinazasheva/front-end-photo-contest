import React, { useContext, useEffect, useState } from 'react';
import JunkieContestView from './JunkieContestView';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from './Styles/useStyles';
import BASE_URL from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';

const ViewOpen = () => {
  const { token } = useContext(AuthContext);
  const [contestOpenData, setContestOpenData] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    fetch(`${BASE_URL}/contests/open`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.length > 0) {
        setContestOpenData(data);
      }
    })
  }, []);

  const transformedOpenContests = contestOpenData.map((contest) => {
    return <JunkieContestView key={contest.contestId} {...contest} />
  });

  return (
    <>
      <br />
      <br />
      <Container className={classes.cardGrid} maxWidth="md">
        <Typography variant="h4" align="center" color="textSecondary" paragraph>
          All open contests
        </Typography>
        {contestOpenData.length > 0 ? <Grid container spacing={4}> {transformedOpenContests} </Grid>
      : (
        <Typography variant="h8" align="center" color="textSecondary" paragraph>
          There are no active contests.
        </Typography>)}
      </Container>
    </>

  );
}

export default ViewOpen;
