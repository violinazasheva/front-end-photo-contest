import React, { useContext, useEffect, useState } from 'react';
import JunkieContestView from './JunkieContestView';
import NavContext from '../../../providers/NavContext'
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from './Styles/useStyles';
import BASE_URL from '../../../common/constants';
import AuthContext from '../../../providers/AuthContext';

const ViewFinished = (props) => {
  const { user, token } = useContext(AuthContext);
  const [contestData, setContestData] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    fetch(`${BASE_URL}/contests/finished`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.length > 0) {
        setContestData(data);
      }
    })
  }, []);

  const transformedContests = contestData.map((contest) => {
    return <JunkieContestView key={contest.contestId} {...contest} />
  });

  return (
    <>
      <br />
      <br />
      <>
        <CssBaseline />
        <main>
          <Container className={classes.cardGrid} maxWidth="md">
            <Typography variant="h4" align="center" color="textSecondary" paragraph>
              Finished contests
            </Typography>
            {contestData.length > 0 ? (
              <Grid container spacing={4}> {transformedContests} </Grid>)
            : (
              <Typography variant="h8" align="center" color="textSecondary" paragraph>
                There are no finished contests yet.
              </Typography>)}
          </Container>
        </main>
      </>
    </>
  );
}

export default ViewFinished;
