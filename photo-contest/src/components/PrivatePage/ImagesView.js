import React, { useContext, useState, useEffect } from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import imageViewStyle from '../PrivatePage/OrganizerPage/Styles/imageViewStyle';
import AuthContext from '../../providers/AuthContext';
import BASE_URL from '../../common/constants';
import { Image } from 'antd';
import Button from 'react-bootstrap/Button';
import IconButton from '@material-ui/core/IconButton';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import CommentIcon from '@material-ui/icons/Comment';
import ViewReviews from '../PrivatePage/JunkiePage/ViewReviews';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
const ImagesView = () => {

  const { token } = useContext(AuthContext);
  const classes = imageViewStyle();
  const [allPhotos, setAllPhotos] = useState([]);

  const [show, setShow] = useState(false);
  const [showReviewsForPhotoWithId, setShowReviewsForPhotoWithId] = useState(0);
  const [photoDescription, setPhotoDescription] = useState(0);
  const [showDesc, setShowDesc] = useState(false);

  const handleClose = () => {
    setShow(false);
    setShowReviewsForPhotoWithId(0);
    setShowDesc(false);
    setPhotoDescription(0);
  };

  const handleshowReviewsForPhotoWithId = (photoId) => {
    setShow(true);
    setShowReviewsForPhotoWithId(photoId);
  }

  const handleViewDesc = (photoId) => {
    setShowDesc(true);
    setPhotoDescription(photoId);
  }
  useEffect(() => {
    fetch(`${BASE_URL}/photos/photoHistory`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response.json())
    .then(data => {
      setAllPhotos(data)
    })
    .catch((err) => alert(err.message));
  }, []);


  const transformedPhotos = allPhotos.map((photo) => {
    return (
      <GridListTile key={photo.photo_id}>
        <Image src={`${BASE_URL}/public/${photo.image_url}`} alt={photo.title} />
        <GridListTileBar
          title={photo.title} 
          actionIcon={
            <>
              <IconButton aria-label="show-photo-description" className={classes.icon} onClick={(e) => {e.preventDefault(); handleViewDesc(photo.photo_id)}}>
                <InfoOutlinedIcon />
              </IconButton>
              <IconButton aria-label={`info about ${photo.title}`} className={classes.icon} onClick={(e) => {e.preventDefault(); handleshowReviewsForPhotoWithId(photo.photo_id)}}>
                <CommentIcon label="review" />
              </IconButton>
            </>
          }
        />
        {photo.photo_id === showReviewsForPhotoWithId ? 
          <div>
            <Modal isOpen={show} className='className'>
              <ModalHeader>Reviews</ModalHeader>
              <ModalBody>
                <ViewReviews photoId={photo.photo_id} />
              </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={handleClose}>Close</Button>
              </ModalFooter>
            </Modal>
          </div> : null}
        {photo.photo_id === photoDescription ? 
          <div>
            <Modal isOpen={showDesc} className='className'>
              <ModalHeader>Description</ModalHeader>
              <ModalBody>
                {photo.story}
              </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={handleClose}>Close</Button>
              </ModalFooter>
            </Modal>
          </div> : null}
      </GridListTile>
    );
  });

  return (
    <>
      <div className={classes.root}>
        <GridList cellHeight={400} cellWidth={200} className={classes.gridList}>
          {transformedPhotos.length > 0 ? transformedPhotos : <div>You do not have a contest in final phase.</div>}
        </GridList>
      </div>
      <br /><br /><br />
    </>
  );
}

export default ImagesView;
