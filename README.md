# **Front-end Photo Contest Final Project Team 7**


This is the repo of the frontend part of the our project. You can find the backend part [here](https://gitlab.com/antoninad/back-end-photo-contest).

To run our project locally, clone **both** backend and frontend repositories in separate VSCode windows.
The project was build, using Express.js, as backend service and React as frontend. 

## How to run the backend?

1. Run `npm install` in the main folder directory.

2. Then in `config.js` file, which is on root level, enter your details as shown below. 

```js
export const DB_CONFIG = {
  host: 'localhost',
  port: 3306,
  user: '', // your user here
  password: '', // your password here
  database: 'mydb'
};
```

3. Find the file `contest_db.sql`, which is also on the root level and copy/paste it in MySQL Workbench.

4. Run `npm run start:dev` to run the application locally. Now you can choose between two options - test our functionalities in Postman (visit `[localhost:4001]` to explore) or dirrectly to run our frontend part.

## How to run the frontend?

1. Open photo-contest directory.

2. Run `npm install` in the main folder directory.

3. Run `npm start` to run the application locally and visit `[localhost:3000]` to explore.

#

## Project Overview
**AV Photo Contest** is a Single-page application, which has two main parts:
* **Organizational** - the application owners can:
  *  **organize contests**
     *  open - every user can join and upload photo in them;
     *  invitational - organizators decide who will be invited to join and only those users can join and upload photos;
  * **invite users with certain rank as a jury**
  * **rate photos and create reviews.**
* For **Photo Junkies** - everyone is welcomed to register and participate in contests.

### **Public Part**
This part is the landing page of the website. User can sign in or sign up. 

### Homepage
![public1](./README-Photos/public1.png)


The idea of this page is to show our future users what to expect from our website and provoke their interest, in order to join us.
![public2](./README-Photos/public2.png)


### Sign up
![signup](./README-Photos/signup.png)

### Sign in
![signin](./README-Photos/signin.png)


### **Private Part**
The logged-in users see the Junkie's or Organizer's Dashboard according to their role. Roles can be 'Junkie' or 'Organizer'. 'Organizer' role is predefined and only the owners of the website can have it. Every other user is with 'Junkie' role. For 'Junkies' scoring is implemented and the can have different ranks, depending on their current points. For ranks, you can read more later or click [here](#ranking).

It is key to remember, that every contest has **three phases**:
* **Phase I** - Users upload photos.
* **Phase II** - Jury (all organizers and invited as jury junkies (if any)) can review and rate every photo in the contest once.
* **Final Phase** - Winning photos are announced. There are 1st - gives 50 points to the user, 2nd - 35 points and 3rd place - 20points. All participants of the contest view reviews and votes for all photos in the contest.

### Organizer's Dashboard
First thing organizers see is the contests in Phase II. From the navigation bar, they can visit contests in different phases and even organize new. There is a quick way to see all the users, ordered by ranking. Functionality to change avatar is also available not only for organizers but also for regular users.

![org-dashboard](./README-Photos/O.dashboard.png)


Organizators can write review in 3 easy steps.


![write-review](./README-Photos/write-review.png)


### Junkie's Dashboard
On Junkie's dashboard are visualized all *open* contests (Phase I). There is button 'Join' if junkie has not joined the contest and button 'upload' when junkie has joined the contest but did not upload a photo.

![junkie-dashboard](./README-Photos/J.dashboard.png)

During Phase II junkies can see the contests in which are participants and the photos of other users, but can not see their reviews until Final Phase. 

Junkie can view his rank and current points.
In Junkies profile all photos, which junkie has uploaded are visible, no matter in which phase the contest is. 

![scoring-profile](./README-Photos/J.profile.png)

![photo-history](./README-Photos/J.photo-history.png)


### Ranking
* (0-50) points – Junkie
* (51 – 150) points – Enthusiast
* (151 – 1000) points – Master (can now be invited as jury)
* (1001 – infinity) points – Wise and Benevolent Photo Dictator (can still be jury)

#

### This is only part of our website, so if you are eager to see more, please follow the [instructions](#how-to-run-the-backend). Thank you for your attention :))

#

### How to find us?
#### Antonina Dermendjieva - antoninad22@gmail.com
#### Violina Zasheva - violinazasheva@gmail.com